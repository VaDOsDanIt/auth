import React, {Component} from 'react';
import {connect} from "react-redux";
import {Switch, Redirect, Route, Router} from "react-router-dom";
import RegisterPage from "./components/pages/RegisterPage";
import PrivateRoute from "./helpers/PrivateRoute";
import SuperSecretInfo from "./components/pages/SuperSecretInfo";
import {authActions} from "./actions";
import "./scss/styles.scss";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            success: false
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.Auth && this.state.success !== nextProps.Auth.success) {
            this.setState(() => {
                return {
                    success: nextProps.Auth.success
                }
            })
        }
    }

    render() {
        return (
            <div>
                <Switch>
                    <PrivateRoute exact={true} path="/"
                                  component={() => (<SuperSecretInfo/>)}/>

                    <Route exact path="/auth" component={() => (<RegisterPage/>)}/>
                </Switch>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {Auth} = state;
    return {
        Auth
    }
};

export default connect(mapStateToProps)(App);
