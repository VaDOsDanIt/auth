import {authServices} from "../services";
import * as constants from "../constants/index";

const login = (email, password) => {
    return dispatch => {
        authServices.login(email, password)
            .then(
                result => {
                    if (result.data.token) {
                        dispatch({type: constants.USER_LOGIN_SUCCESS, payload: {user: result.data.user, success: result.data.success, token: result.data.token}})
                    } else {
                        dispatch({type: constants.USER_LOGIN_FAILTURE, payload: {message: result.data.message}})

                    }
                },
                error => dispatch({type: constants.USER_LOGIN_FAILTURE})
            )
    }
};

const register = (email, password) => {
    return dispatch => {
        authServices.register(email, password)
            .then(
                result => dispatch({type: constants.USER_REGISTER_SUCCESS}),
                error => dispatch({type: constants.USER_REGISTER_FAILTURE})
            )
    }
};

const check = () => {

    return dispatch => {
      authServices.check(JSON.parse(localStorage.getItem("token")))
          .then(
              result => dispatch({type: constants.USER_CHECK_SUCCESS, payload: {user: result.data.user, token: result.data.token, success: result.data.success}}),
              error => dispatch({type: constants.USER_CHECK_FAILTURE})
          )
  }
};

const logout = () => {
  return dispatch => {
      dispatch({type: constants.USER_LOGOUT});
  }
};

export const authActions = {
    login,
    register,
    check,
    logout
};