import React, {Component} from 'react';
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import "../../scss/registerPage.scss";
import {authActions} from "../../actions";

class RegisterPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };

        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
    }

    onChangeEmail(value) {
        this.setState(() => {
            return {
                email: value,
            }
        })
    }

    onChangePassword(value) {
        this.setState(() => {
            return {
                password: value
            }
        })
    }


    render() {
        const {dispatch} = this.props;
        return (
            <div className="register-page">
                <form className="form-group">
                    <label className="form-label">
                        Email
                        <input className="label-input" onChange={(e) => {
                            this.onChangeEmail(e.target.value);
                        }} placeholder="Введите email" type="text"/>
                    </label>
                    <label className="form-label">
                        Пароль
                        <input className="label-input" onChange={(e) => {
                            this.onChangePassword(e.target.value);
                        }} placeholder="Введите пароль" type="password"/>
                    </label>
                    <button className="register-button" onClick={(e) => {
                        e.preventDefault();
                        dispatch(authActions.register(this.state.email.replace(/\s+/g, ''), this.state.password));
                    }}>Регистрация
                    </button>

                    <button className="login-button" onClick={(e) => {
                        e.preventDefault();
                        dispatch(authActions.login(this.state.email.replace(/\s+/g, ''), this.state.password));
                    }}>Войти
                    </button>
                </form>
                {this.props.Auth && this.props.Auth.success === true && <Redirect to="/"/>}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {Auth} = state;
    return {
        Auth
    }
};

export default connect(mapStateToProps)(RegisterPage);