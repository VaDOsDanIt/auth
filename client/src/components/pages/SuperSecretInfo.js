import React, {Component} from 'react';
import moment from "moment";
import {connect} from "react-redux";
import {authActions} from "../../actions";

class SuperSecretInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
          userName: ''
        };

        this.handleChangeUserName = this.handleChangeUserName.bind(this);
    }

    handleChangeUserName(e) {
        this.setState({userName: e.target.value});
    }

    render() {
        return (
            <div>
                <h1>HELLO WORLD</h1>
                <p>{this.props.Auth && this.props.Auth.email}</p>
                <p>Register
                    date: {this.props.Auth && moment(this.props.Auth.registerDate).format("DD MMMM YYYY H:mm")}</p>

                <button onClick={() => {
                    this.props.dispatch(authActions.logout());
                }}>EXIT!
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {Auth} = state;
    return {
        Auth
    }
};

export default connect(mapStateToProps)(SuperSecretInfo);