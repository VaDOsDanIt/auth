import React, {useEffect} from 'react';
import {Route, Redirect} from 'react-router-dom';
import {connect} from "react-redux";
import {authActions} from "../actions";

const PrivateRoute = ({component: Component, handleChildFunc, ...rest}) => {
    useEffect(() => {
        rest.dispatch(authActions.check());
    }, []);

    return <Route {...rest} render={(props) => (
        rest.Auth && rest.Auth.success === true
            ? <Component {...props} handleChildFunc={handleChildFunc}/>
            : (rest.Auth.loading ? <p>Loading...</p> : <Redirect to='/auth'/>)
    )}
    />
}

const mapStateToProps = (state) => {
    const {Auth} = state;
    return {
        Auth
    }
}

export default connect(mapStateToProps)(PrivateRoute);

