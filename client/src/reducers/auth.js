import * as constants from "../constants/index";

const initialState = {
    success: false,
    loading: true
};

export const Auth = (state = initialState, action) => {
    switch (action.type) {
        case constants.USER_LOGIN_SUCCESS:
            localStorage.setItem("token", JSON.stringify(action.payload.token));
            return {
                ...state,
                ...action.payload.user,
                success: action.payload.success,
                loading: false
            };
        case constants.USER_CHECK_SUCCESS:
            localStorage.setItem("token", JSON.stringify(action.payload.token));
            return {
                ...state,
                ...action.payload.user,
                success: action.payload.success,
                loading: false
            };

        case constants.USER_LOGOUT:
            localStorage.clear();
            return {
                success: false,
                loading: false
            };
        default:
            return state;
    }
};