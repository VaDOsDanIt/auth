import {combineReducers} from "redux";

import {Auth} from "../reducers/auth";

export const reducers = combineReducers({Auth});