import axios from "axios";

const origin = "https://server-for-test.herokuapp.com";
// const origin = "http://localhost:8000";

const login = (email, password) => {
    return axios.post(origin + "/api/auth/login", {
        email,
        password
    });
};

const register = (email, password) => {
    return axios.post(origin + "/api/auth/register", {
        email,
        password
    })
};

const check = (token) => {
    return axios.post(origin + "/api/auth/getUser", {
       token
    });
};

export const authServices = {
  login,
  register,
  check
};