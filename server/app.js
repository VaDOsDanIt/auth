const express = require("express");
const cors = require("cors");
const config = require("config");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const authApi = require("./routes/auth");
const userApi = require("./routes/user");

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use("/api/auth", authApi);
app.use("/api/user", userApi);

const PORT = process.env.PORT || config.get("defaultPort");

const start = async () => {
    await mongoose.connect(config.get('mongoURI'), {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    }, err => {
        if (err) throw err;
        console.log("DB is connected");
    });

    mongoose.connection.on('error',function (err) {
        console.log('Mongoose default connection error: ' + err);
    });

    app.listen(PORT, () => {
        console.log("App is started on " + PORT + " port.");
    });

    process.on('SIGINT', function() {
        mongoose.connection.close(function () {
            console.log('Mongoose default connection disconnected.');
            process.exit(0);
        });
    });
}

start();
