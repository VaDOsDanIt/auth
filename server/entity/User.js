const mongoose = require("mongoose");

const schema = new mongoose.Schema({
    email: {type: String, required: true},
    password: {type: String, required: true},
    registerDate: {type: Date, default: new Date(),
    userName: {type: String}}
});

module.exports = mongoose.model("User", schema);