const jwt = require('jsonwebtoken');
const config = require('config');

const generateToken = (user) => {
    const data = {
        email: user.email,
    };
    const secret = config.get('jwtSecret');
    const expiration = config.get('jswExpires');

    return jwt.sign(data, secret, {expiresIn: expiration});
};

const verifyToken = (token) => {
    return jwt.verify(token, config.get('jwtSecret'), (err, decode) => {
        return {err: err === null, decode};
    });
};

module.exports = {generateToken, verifyToken};
