const {verifyToken, generateToken} = require("../helpers/jwt");
const express = require("express");
const validator = require("email-validator");
const User = require("../entity/User");

const router = express.Router();

router.post("/register", (req, res) => {
    const {email, password} = req.body;

    if (email && password) {
        if (password.length < 8) {
            res.send({message: "Password must have min 8 chars"});
        } else if (!validator.validate(email)) {
            res.send({message: "Email is wrong"});
        } else {
            User.find({email: email.toLowerCase()}, (err, result) => {
                if (err) res.send({message: "На сервере произошла ошибка. Попробуйте повторить операцию позже."});
                else {
                    if (result.length > 0) {
                        res.send({message: "Данный пользователь уже существует."});
                    } else {
                        const user = new User({
                            email: email.replace(/\s+/g, '').toLowerCase(),
                            password
                        });
                        user.save();
                        res.send({message: "Успешная регистрация!"})
                    }
                }
            });
        }
    } else {
        res.send({message: "Email or password is wrong"})
    }
});

router.post("/login", (req, res) => {
    const {email, password} = req.body;

    if (email && password && validator.validate(email)) {
        User.find({email: email.replace(/\s+/g, '').toLowerCase()}, (err, result) => {
            if (err) res.send({message: "На сервере произошла ошибка. Попробуйте повторить операцию позже."});
            else {
                if (result.length === 0) res.send({message: "Почта или пароль не верны."});
                else {
                    const [user] = result;

                    if (user.password === password) {
                        res.send({
                            token: generateToken(user),
                            success: true,
                            user: {email: result.email, registerDate: result.registerDate}
                        });
                    } else {
                        res.send({message: "Почта или пароль не верны."});
                    }
                }
            }
        })
    } else {
        res.send({message: "Ошибка почты или пароля."})
    }
});

router.post("/getUser", (req, res) => {
    const {token} = req.body;

    const verifiedToken = token && verifyToken(token);

    if (token && verifiedToken && verifiedToken.err) {
        User.findOne({email: verifiedToken.decode.email}, (err, user) => {
            if (!err && user) {
                res.send({
                    user: {registerDate: user.registerDate, email: user.email},
                    token: generateToken(user),
                    success: true
                })
            }
        })
    } else {
        res.send({success: false});
    }

});

module.exports = router;