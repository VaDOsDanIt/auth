const {verifyToken, generateToken} = require("../helpers/jwt");
const express = require("express");
const User = require("../entity/User");

const router = express.Router();


router.get("/get", (req, res) => {
    const {token} = req.body;

    const verifiedToken = verifyToken(token);

    if (verifiedToken.err) {
        User.findOne({email: verifiedToken.decode.email}, (err, user) => {
            if (!err && user) {
                res.send(user);
            }
        })
    } else {
        console.log("err")
    }
});

module.exports = router;
